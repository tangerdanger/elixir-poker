# Poker

## Status
Very WIP.

The game is built using Phoenix LiveView and Surface (https://surface-ui.org) to run it as a LiveView page.

In the backend, each game has the concept of a Deck (GenServer) that exists until exhausted.

We can keep player's hands (and other data in the future) by holding their hand state in an Agent and committing the PID of the agent to mnesia for fast lookup

To start your Poker server:

  * Install dependencies with `mix deps.get`
  * Install amnesia database using `mix amnesia.create -d PokerDatabase --disk`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
