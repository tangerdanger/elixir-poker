defmodule PokerWeb.Live.Components.Card do
  use Surface.Component

  prop suit, :string
  prop name, :string
  prop action, :event, default: nil

  @card_id_to_html %{}

  def render(assigns) do
    ~F"""
    <div class="column is-one-quarter">
      <button :on-click={@action} value={"#{@suit}_#{@name}"}>
        <div class={"card", rank_to_class(@name) |> apply_rank(), format_suit(@suit)}>
          <span class="rank">{rank_to_class(@name, :upcase)}</span>
          <span class="suit">&{format_suit(@suit)};</span>
        </div>
      </button>
    </div>
    """
  end

  def rank_to_class(rank, mode \\ :downcase) do
    case rank |> String.downcase() do
      "ace" -> "a" |> apply_case(mode)
      "two" -> 2
      "three" -> 3
      "four" -> 4
      "five" -> 5
      "six" -> 6
      "seven" -> 7
      "eight" -> 8
      "nine" -> 9
      "ten" -> 10
      "jack" -> "j" |> apply_case(mode)
      "queen" -> "q" |> apply_case(mode)
      "king" -> "k" |> apply_case(mode)
    end
  end

  def format_suit(suit) do
    case suit do
      "Diamonds" -> "diams"
      s -> s |> String.downcase()
    end
  end

  def apply_case(num, mode) do
    case mode do
      :downcase -> num
      :upcase -> String.upcase(num)
    end
  end

  def apply_rank(num) do
    "rank-#{num}"
  end
end
