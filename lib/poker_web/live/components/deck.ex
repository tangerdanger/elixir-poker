defmodule PokerWeb.Live.Components.Deck do
  use Surface.Component

  alias PokerWeb.Live.Components.Card, as: CardComponent
  alias Poker.Components.Card

  prop cards, :list
  prop in_play, :map
  prop game_id, :string
  prop hand, :list

  def render(assigns) do
    ~F"""
    <div id="play-field" class="deck-view">
      <h2 class="subtitle">In Play</h2>
      <div class="play-area columns">
        {#for {player, players_in_play} <- @in_play}
        <div class="players_play is-one-quarter">
          {player}
          <div class="in-play columns is-multiline is-mobile">
            {#for card <- players_in_play}
          <CardComponent suit={card.suit} name={card.type} />
            {#else}
              No cards in play
            {/for}
          </div>
        </div>
        {/for}
      </div>
    </div>

    <div id="hand" class="deck-view">
      <h2 class="subtitle is-4">Deck Count: {length(@cards)}</h2>
      <h2 class="subtitle">Hand</h2>
      <h3 class="subtitle is-4">Count: {length(@hand)}</h3>
      <div class="hand columns is-multiline is-mobile">
        {#for card <- @hand}
      <CardComponent action="play-card" suit={card.suit} name={card.type} />
        {#else}
          No cards in your hand
        {/for}
      </div>
    </div>
    """
  end
end
