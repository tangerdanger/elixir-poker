defmodule PokerWeb.Live.GameLive do
  use Surface.LiveView

  require Amnesia
  require Amnesia.Helper
  require Exquisite
  require PokerDatabase.Player

  alias Poker.Components.Deck
  alias PokerWeb.Live.Components.Deck, as: DeckComponent
  alias PokerDatabase.Player

  data cards, :list
  data in_play, :map
  data state, :map
  data game_id, :string
  data hand, :pid

  def via_tuple(game_id) do
    {:via, Registry, {Poker.DeckRegistry, game_id}}
  end

  defp generate_game_id do
    Ecto.UUID.generate()
  end

  @impl true
  def mount(params, session, socket) do
    {:ok, assign(socket, player_id: Map.get(session, "player_id"))}
  end

  @impl true
  def handle_params(%{"gid" => game_id} = _params, _uri, socket) do
    # Resume existing game
    :ok = Phoenix.PubSub.subscribe(Poker.PubSub, game_id)
    {:noreply, assign_deck(socket, game_id) |> assign_hand()}
  end

  @impl true
  def handle_params(_params, _uri, socket) do
    # If the URL has no game_id, then we create a new  one
    game_id = generate_game_id()

    # Start a dynamically supervised deck instance
    # This makes sure we have one canonical source for all players in a game
    {:ok, _pid} =
      DynamicSupervisor.start_child(Poker.DeckSupervisor, {Deck, name: via_tuple(game_id)})

    # Redirect to the same page, with a gid attached
    {:noreply,
     push_redirect(
       socket,
       to: PokerWeb.Router.Helpers.live_path(socket, PokerWeb.Live.GameLive, gid: game_id)
     )}
  end

  def assign_deck(socket, game_id) do
    deck = GenServer.call(via_tuple(game_id), :deck)

    assign(socket,
      cards: deck.cards,
      in_play: deck.in_play,
      count: deck.count,
      game_id: game_id
    )
  end

  def get_hand(hand_pid) do
    Agent.get(hand_pid, fn state -> state end)
  end

  def assign_hand(%{assigns: %{player_id: pplayer_id, game_id: ggame_id}} = socket) do
    Amnesia.transaction do
      # Player.where player_id == player_id and game_id == game_id
      Player.where(player_id == pplayer_id and game_id == ggame_id)
      |> Amnesia.Selection.values()
      |> List.first()
      |> case do
        nil ->
          {:ok, pid} = Agent.start_link(fn -> [] end)

          %Player{player_id: pplayer_id, game_id: ggame_id, hand_id: pid}
          |> Player.write!()

          assign(socket, :hand, pid)

        player ->
          assign(socket, :hand, player.hand_id)
      end
    end
  end

  def update_hand(%{assigns: %{hand: hand_pid}} = socket, card, player_id, current_player_id)
      when current_player_id == player_id do
    Agent.update(hand_pid, &([card] ++ &1))
    socket
  end

  def update_hand(socket, _card, _player_id, _current_player_id) do
    socket
  end

  def remove_card_from_hand(%{assigns: %{hand: hand_pid}} = socket, card_id) do
    Agent.update(
      hand_pid,
      &Enum.reject(&1, fn e ->
        Poker.Components.Card.id(e) == card_id
      end)
    )

    socket
  end

  @impl true
  def handle_event("draw", _value, %{assigns: %{game_id: game_id, player_id: player_id}} = socket) do
    draw = Deck.draw(via_tuple(game_id))

    {:noreply,
     case draw do
       nil ->
         socket

       new_card ->
         :ok = Phoenix.PubSub.broadcast(Poker.PubSub, game_id, {:update, {player_id, new_card}})
         socket
     end}
  end

  @impl true
  def handle_event(
        "play-card",
        %{"value" => card_id},
        %{assigns: %{game_id: game_id, player_id: current_player_id}} = socket
      ) do
    # Update our deck and add our played card to the playing field
    GenServer.cast(via_tuple(game_id), {:play, card_id, current_player_id})
    :ok = Phoenix.PubSub.broadcast(Poker.PubSub, game_id, {:update, :play, card_id})

    {:noreply, socket}
  end

  @impl true
  def handle_info(
        {:update, {player_id, card}},
        %{assigns: %{game_id: game_id, player_id: current_player_id}} = socket
      ) do
    {:noreply,
     socket
     |> assign_deck(game_id)
     |> update_hand(card, player_id, current_player_id)}
  end

  @impl true
  def handle_info(
        {:update, :play, card_id},
        %{assigns: %{game_id: game_id, player_id: current_player_id}} = socket
      ) do
    {:noreply,
     socket
     |> remove_card_from_hand(card_id)
     |> assign_deck(game_id)}
  end

  # @impl true
  # def terminate(reason, socket) do
  #  IO.inspect("TERMINATING")
  #  Agent.stop(socket.assigns.hand)
  # end
end
