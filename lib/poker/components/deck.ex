defmodule Poker.Components.Deck do
  use GenServer, restart: :transient

  alias Poker.Components.Card

  # 10 min timeout
  @timeout 600_000
  @suits ["Spades", "Diamonds", "Clubs", "Hearts"]
  @card_types [
    "Ace",
    "Two",
    "Three",
    "Four",
    "Five",
    "Six",
    "Seven",
    "Eight",
    "Nine",
    "Ten",
    "Jack",
    "Queen",
    "King"
  ]

  defstruct [:cards, count: 52, in_play: %{}]

  def start_link(options) do
    GenServer.start_link(__MODULE__, %Poker.Components.Deck{}, options)
  end

  def state(game) do
    # TODO
    game
  end

  def count(deck) do
    GenServer.call(deck, :count)
  end

  def draw(deck) do
    GenServer.call(deck, :draw)
  end

  def play(deck, card) do
    GenServer.cast(deck, {:draw, card})
  end

  @impl true
  def init(deck) do
    {:ok, deck |> initialise_deck(), @timeout}
  end

  @impl true
  def handle_call(:deck, _from, deck) do
    {:reply, deck, deck}
  end

  @impl true
  def handle_call(:draw, _from, %{cards: [top_card | rest_of_deck], count: count} = state) do
    {:reply, top_card, %{state | cards: rest_of_deck, count: count - 1}}
  end

  @impl true
  def handle_call(:draw, _from, %{cards: [last_card]} = state) do
    {:reply, last_card, %{state | cards: [], count: 0}}
  end

  @impl true
  def handle_call(:draw, _from, %{cards: []} = state) do
    # Empty deck
    {:reply, nil, state}
  end

  @impl true
  def handle_call(:count, _from, state) do
    {:reply, Map.get(state, :count), state}
  end

  @impl true
  def handle_cast({:play, card, player_id}, state) do
    [suit, name] = String.split(card, "_")
    card = %Card{suit: suit, type: name}

    if card in state.cards or card in state.in_play do
      {:noreply, state}
    else
      {:noreply,
       Map.update!(
         state,
         :in_play,
         &Map.merge(&1, %{player_id => [card]}, fn _k, v1, v2 -> v1 ++ v2 end)
       )}
    end
  end

  @impl true
  def handle_info(:timeout, deck) do
    {:stop, :normal, deck}
  end

  def initialise_deck(deck) do
    Map.put(
      deck,
      :cards,
      for suit <- @suits,
          card_type <- @card_types,
          into: [] do
        %Card{suit: suit, type: card_type}
      end
      |> Enum.shuffle()
    )
  end
end
