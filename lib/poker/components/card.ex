defmodule Poker.Components.Card do
  defstruct [:suit, :type]

  def id(card) do
    "#{card.suit}_#{card.type}"
  end
end
