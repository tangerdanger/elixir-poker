use Amnesia

defdatabase PokerDatabase do
  deftable(
    Player,
    [{:id, autoincrement}, :player_id, :game_id, :hand_id],
    type: :ordered_set,
    index: [:player_id, :game_id]
  ) do
  end
end
