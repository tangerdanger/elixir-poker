defmodule CardComponentTest do
  use ExUnit.Case
  use Surface.LiveViewTest
  alias PokerWeb.Live.Components.Card

  @endpoint Endpoint

  test "renders name and suite" do
    html =
      render_surface do
        ~F"""
        <Card suit="Spades" name="Queen" />
        """
      end

    assert html =~ """
           <div class="card">
             Queen of Spades
           </div>
           """
  end
end
